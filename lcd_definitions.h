#define LCD_CONTROLLER_KS0073   0
#define LCD_LINES   4
#define LCD_DISP_LENGTH   16
#define LCD_LINE_LENGTH   0x40 //??
#define LCD_START_LINE1   0x00
#define LCD_START_LINE2   0x40
#define LCD_START_LINE3   0x10
#define LCD_START_LINE4   0x50
#define LCD_WRAP_LINES   0


#define LCD_IO_MODE   1
#define LCD_PORT   PORTF
#define LCD_DATA0_PORT   LCD_PORT
#define LCD_DATA1_PORT   LCD_PORT
#define LCD_DATA2_PORT   LCD_PORT
#define LCD_DATA3_PORT   LCD_PORT
#define LCD_DATA0_PIN   6 //
#define LCD_DATA1_PIN   5
#define LCD_DATA2_PIN   4
#define LCD_DATA3_PIN   1 //
#define LCD_RS_PORT  PORTD //LCD_PORT
#define LCD_RS_PIN   6
#define LCD_RW_PORT   PORTD//LCD_PORT
#define LCD_RW_PIN   7
#define LCD_E_PORT   LCD_PORT
#define LCD_E_PIN   7
